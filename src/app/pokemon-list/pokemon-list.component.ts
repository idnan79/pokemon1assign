import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Contact } from '../models/pokemon.model';

import { ContactsService } from '../services/pokemons.service';
import { SelectedContactService } from '../services/selected-pokemon.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './pokemon-list-component.html',
  styleUrls: ['./pokemon-list.component.css'],
})
export class ContactListComponent implements OnInit {
  // [x: string]: any;
  private _contacts: Contact[] = [];
  page = 1;

  totalPokemons!: number;
  constructor(
    private readonly contactService: ContactsService,
    private readonly selectedContactService: SelectedContactService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.contactService.getPokemons().subscribe((response: any) => {
      this.totalPokemons = response.count;

      response.results.forEach((result: { name: string }) => {
        this.contactService
          .getMoreData(result.name)
          .subscribe((response: any) => {
            this._contacts.push(response);
            console.log(this.contacts);
          });
      });
    });
  }
  get contacts(): Contact[] {
    return this._contacts;
  }

  public handleContactClicked(contact: Contact): void {
    this.selectedContactService.setContact(contact);
  }

  checklocal() {
    if (localStorage.getItem('name') === null) {
      this.router.navigate(['/contacts/create']);
    }
  }

  ProfilePage() {
    this.router.navigate(['/contacts/create/profilepage']);
  }
}
