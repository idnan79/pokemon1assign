export interface Contact {
  user: string;
  name: string;
  email: string;
  id: number;
  sprites: spritesValue;
  createdAt: number;
  height: number;
  back_shiny: string;
  types: [];
  moves: [];
  weight: number;
  stats: [];
  base_stat: string;
}

export interface spritesValue {
  back_default: string;
}
