import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Contact } from '../models/pokemon.model';
import { SelectedContactService } from '../services/selected-pokemon.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.css'],
})
export class ProfilePage {
  constructor(
    private router: Router,

    private readonly selectedContactService: SelectedContactService
  ) {}

  get contact(): Contact | null {
    return this.selectedContactService.contact();
  }

  public getStorage() {
    return JSON.parse(localStorage.getItem('pokemon') || '{}');
  }

  public getUser() {
    return localStorage.getItem('name');
  }
  public return() {
    localStorage.clear();
    this.router.navigate(['/contacts/create']);
  }
  public trainer() {
    this.router.navigate(['/contacts']);
  }
  checklocal() {
    if (localStorage.getItem('name') === null) {
      this.router.navigate(['/contacts/create']);
    }
  }
}
