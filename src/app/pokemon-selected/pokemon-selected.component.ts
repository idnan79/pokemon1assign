import { Component } from '@angular/core';
import { Contact } from '../models/pokemon.model';
import { SelectedContactService } from '../services/selected-pokemon.service';

@Component({
  selector: 'app-contact-selected',
  templateUrl: './pokemon-selected.component.html',
  styleUrls: ['./pokemon-selected.component.css'],
})
export class ContactSelectedComponent {
  constructor(
    private readonly selectedContactService: SelectedContactService
  ) {}

  get contact(): Contact | null {
    return this.selectedContactService.contact();
  }
  public getStorage() {
    return JSON.parse(localStorage.getItem('pokemon')!);
  }
}
