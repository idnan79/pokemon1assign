import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contact-create',
  templateUrl: './pokemon-create.page.html',
  styleUrls: ['./pokemon-create.page.css'],
})
export class ContactCreatePage {
  [x: string]: any;

  constructor(private router: Router) {}

  checklocal() {
    if (localStorage.getItem('name') !== null) {
      this.router.navigate(['/contacts']);
    }
  }
  public onSubmit(createForm: NgForm): void {
    console.log('Hello');
    localStorage.setItem('name', createForm.value.name);

    console.log(createForm.value.name);
    //this.router.navigateByUrl('/contacts');
  }
}
