import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Contact } from '../models/pokemon.model';

@Component({
  selector: 'app-contact-list-item',
  templateUrl: './pokemon-list-item.component.html',
  styleUrls: ['./pokemon-list-item.component.css'],
})
export class ContactListItemComponent {
  @Input() contact: Contact | undefined;
  @Output() clicked: EventEmitter<Contact> = new EventEmitter();

  public onContactClicked(): void {
    //notify the parent the item is clicked
    this.clicked.emit(this.contact);
  }
  public getStorage() {
    return JSON.parse(localStorage.getItem('pokemon') || '{}');
  }
}
