import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactsPage } from './pokemons.page/pokemons.page';
import { ContactCreatePage } from './Pokemon-create/pokemon.create.page';
import { ProfilePage } from './profile-display/profile.page';

//conatctsList our contac
//create contact -create a new contact

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'contacts/create',
  },
  {
    path: 'contacts/create',
    component: ContactCreatePage,
  },
  {
    path: 'contacts',
    component: ContactsPage,
  },
  {
    path: 'contacts/create/profilepage',
    component: ProfilePage,
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
