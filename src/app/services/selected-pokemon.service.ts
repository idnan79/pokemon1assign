import { Injectable } from '@angular/core';
import { Contact } from '../models/pokemon.model';

@Injectable({
  providedIn: 'root',
})
export class SelectedContactService {
  [x: string]: any;
  private _contacts: Contact[] = [];
  private _contact: Contact | null = null; ////check this value ----------------------------
  public _localstorage: string[] = [];

  public setContact(contact: Contact) {
    this._contact = contact;
    this._contacts.push(this._contact);
    localStorage.setItem('pokemon', JSON.stringify(this._contacts));
  }

  public contact(): Contact | null {
    return this._contact;
  }
}
