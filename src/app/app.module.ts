import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { ContactSelectedComponent } from './pokemon-selected/pokemon-selected.component';

import { ContactsPage } from './pokemons.page/pokemons.page';
import { ContactCreatePage } from './Pokemon-create/pokemon.create.page';

import { AppRoutingModule } from './app-routing.module';
import { ContactListComponent } from './pokemon-list/pokemon-list.component';
import { ContactListItemComponent } from './pokemon-list-item/pokemon-list-item.component';
import { ProfilePage } from './profile-display/profile.page';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [
    AppComponent,
    ContactSelectedComponent,
    ContactListComponent,
    ContactsPage,
    ContactCreatePage,
    ContactListItemComponent,
    ProfilePage,
  ], //components here
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    NgxPaginationModule,
  ], //models here
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
